package pl.sda.wprowadzenie.OOP.Ptak;

//Stwórz klasę Ptak która posiada metodę ‘śpiewaj():void’ która wypisuje ‘ćwir ćwir’.
//
//a) stwórz klasę dziedziczącą Bocian która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘kle kle’
//b) stwórz klasę dziedziczącą Kukułka która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘kuku kuku’
//c) stwórz klasę dziedziczącą Sowa która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘hu hu’
//
//    Stwórz kilka instancji każdej z klas i użyj metody śpiwaj();
//
//
//    Stwórz kilka instancji każdej klasy i przetestuj działanie metod ‘śpiewaj():void’

public class Sowa extends Ptak{

    @Override
    public void spiewaj() {
        //super.spiewaj();
        System.out.println("hu hu");

    }
}
