package pl.sda.wprowadzenie.metodyZadania;

import java.util.Scanner;

public class MetodyStatyczneKalkulator {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String slowo = scanner.next();

        //String dzialanie = "*";
        //String dzialanie2 = "+";
        //String dzialanie3 = "-";
        //String dzialanie4 = "/";

        System.out.println("Podaj pierwszą Liczbę");
        int a = scanner.nextInt();

        System.out.println("Podaj Drugą Liczbę");
        int b = scanner.nextInt();

        switch (slowo) {
            case "+":
                dodawanie(a, b);
                //
                break;
            case "-":
                odejmowanie(a, b);
                //
                break;
            case "*":
                mnozenie(a, b);
                //
                break;
            case "/":
                dzielenie(a, b);
                //
                break;
            default:
                System.out.println("Niepoprawne działanie!");
        }

        // znak na indeksie zerowym (pierwszy znak)
        char pierwszyZnak = slowo.charAt(0);
    }

    public static void dodawanie(int a, int b) {
        System.out.println(a + b);
    }


    public static void odejmowanie(int a, int b) {
        System.out.println(a - b);
    }

    public static void mnozenie(int a, int b) {
        System.out.println(a * b);
    }

    public static void dzielenie(int a, int b) {
        System.out.println(a / b);
    }
}


