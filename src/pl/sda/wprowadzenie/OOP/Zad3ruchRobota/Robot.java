package pl.sda.wprowadzenie.OOP.Zad3ruchRobota;

public class Robot {
    private int poziomBaterii;
    private String nazwa;
    private boolean czyWlaczony;

    public Robot(int poziomBaterii, String nazwa, boolean czyWlaczony) {
        this.poziomBaterii = poziomBaterii;
        this.nazwa = nazwa;
        this.czyWlaczony = czyWlaczony;
    }

    public void poruszRobotem(RuchRobota ruchRobota) {
        if (czyWlaczony) {
            if (poziomBaterii > ruchRobota.getPotrzebnaMoc()) {
                poziomBaterii -= ruchRobota.getPotrzebnaMoc();
                System.out.println("Wykonano ruch!");
            } else {
                System.out.println("Niedostateczny poziom baterii!");
            }
        } else {
            System.out.println("Włącz robota!");
        }
    }

    public void naładujRobota() {
        if (czyWlaczony) {
            System.out.println("Wyłącz robota podczas ładowania");
        } else {
            poziomBaterii = 100;
        }
    }

    public void włącz() {
        czyWlaczony = true;
    }

    public void wyłącz() {
        czyWlaczony = false;
    }

    public int getPoziomBaterii() {
        return poziomBaterii;
    }

    public void setPoziomBaterii(int poziomBaterii) {
        this.poziomBaterii = poziomBaterii;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public boolean isCzyWlaczony() {
        return czyWlaczony;
    }

    public void setCzyWlaczony(boolean czyWlaczony) {
        this.czyWlaczony = czyWlaczony;
    }
}