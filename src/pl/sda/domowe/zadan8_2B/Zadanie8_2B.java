package pl.sda.domowe.zadan8_2B;

import java.util.Scanner;

public class Zadanie8_2B {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int liczba;
        int[] tablicaLiczb = new int[20000];
        int licznikIndeksu = 0;
        do {
            liczba = scanner.nextInt();
//            for (int i = 0; i < tablicaLiczb.length; i++) {
//                tablicaLiczb[i] = liczba;
//            }
            tablicaLiczb[licznikIndeksu] = liczba;
            licznikIndeksu++;

        } while (liczba != 0);

        int min, max;

        min = tablicaLiczb[0];
        max = tablicaLiczb[0];
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            if (tablicaLiczb[i] < min) {
                min = tablicaLiczb[i];
            }
            if (tablicaLiczb[i] > max) {
                max = tablicaLiczb[i];
            }
        }

        System.out.println(min + max);

        int suma = 0;
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            suma += tablicaLiczb[i];
        }

        System.out.println((suma / (double) (licznikIndeksu - 1)));

    }
}

