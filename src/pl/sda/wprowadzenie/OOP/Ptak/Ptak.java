package pl.sda.wprowadzenie.OOP.Ptak;

public class Ptak {
    public static void main(String[] args) {

 //Ptak ptak = new Sowa();
        //ptak to referencja --> Sowa to obiekt
        //ptak.spiewaj();
        //ptak.powiedzCos();
        ptak.spiewaj();
        if (ptak instanceof Sowa) {
            Sowa sowa = (Sowa) ptak;
            sowa.powiedzCos();
        }
    }
    public void spiewaj () {
        System.out.println("ćwir ćwir");

    }
    //Stwórz klasę Ptak która posiada metodę ‘śpiewaj():void’ która wypisuje ‘ćwir ćwir’.
    //
    //a) stwórz klasę dziedziczącą Bocian która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘kle kle’
    //b) stwórz klasę dziedziczącą Kukułka która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘kuku kuku’
    //c) stwórz klasę dziedziczącą Sowa która nadpisuje klasę ‘śpiewaj():void’ i wypisuje ‘hu hu’
    //
    //    Stwórz kilka instancji każdej z klas i użyj metody śpiwaj();
    //
    //
    //    Stwórz kilka instancji każdej klasy i przetestuj działanie metod ‘śpiewaj():void’
}
