package pl.sda.wprowadzenie.zad3.zad3.dzielenie;

import java.sql.SQLOutput;

public class Zadanie3_Dzielenie {
    public static void main(String[] args) {
        int a = 1, b = 2;
        float c = 3;

        System.out.println(a / 2);
        System.out.println(a / b);
        System.out.println(1 / 1);
        System.out.println(a / (b / c));

        c = a / b;

        char znak = 'a';
        System.out.println(a / (b / c));

    }
}
