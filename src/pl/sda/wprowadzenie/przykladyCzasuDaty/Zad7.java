package pl.sda.wprowadzenie.przykladyCzasuDaty;

// Napisz aplikację która po odpowiedzi użytkownika na pytania:
//         1) Podaj datę urodzenia w formacie: yyyy-MM-dd":
//         2) Kobieta, czy mężczyzna (k/m) - mężczyźni żyją średnio o 10 lat krócej
//         3) Czy palisz papierosy? y/n   (palący żyją średnio 9 lat i 3 miesiące krócej)
//         4) Czy żyjesz w stresie? (jeśli tak, istnieje 10% prawdopodobieństwo, że umrzesz na zawał serca w wieku 60 lat)
//
//         ...poda użytkownikowi przybliżoną datę śmierci. Załóż, że "startowa" długość życia to 100 lat. Aplikacja ma podawać datę z        dokładnością do miesiąca. Możesz dodać dodatkowe warunki. Możesz użyc klasy java.time.Period
//         * Wyświetl na końcu, ile użytkownik stracił czasu na bezsensownym sprawdzaniu daty śmierci
//         ** Dodaj nowe dodatkowe warunki i randomizuj niektóre wartości

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.Random;

public class Zad7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę urodzenia:");

        String dataString = scanner.next();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateStarting = LocalDate.parse(dataString, formatter);

        LocalDate date = dateStarting.plusYears(100);

        String odpowiedz;


        do {
            System.out.println("Jesteś kobietą czy mężczyzną (m/k)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("m") && !odpowiedz.equals("k"));

        if (odpowiedz.equals("m")) {
            date = date.minusYears(10);
        }

        do {
            System.out.println("Czy palisz papierosy (t/n)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("t") && !odpowiedz.equals("n"));

        if (odpowiedz.equals("t")) {
            date = date.minusYears(9).minusMonths(3);
        }

        do {
            System.out.println("Czy żyjesz w stresie (t/n)?");
            odpowiedz = scanner.next();
        } while (!odpowiedz.equals("t") && !odpowiedz.equals("n"));

        if (odpowiedz.equals("t")) {
            Random random = new Random();
            int wylosowana = random.nextInt(100);
            if (wylosowana < 10) {
                date = dateStarting.plusYears(60);
            }
        }

        System.out.println("Umrzesz : " + date);
    }
}

