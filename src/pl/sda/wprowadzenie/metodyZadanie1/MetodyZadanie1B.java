package pl.sda.wprowadzenie.metodyZadanie1;

public class MetodyZadanie1B {
          public static void main(String[] args) {
            int a = 6;
            drukujParzystoscLiczby(a);

            System.out.println(czyLiczbaJestParzysta(a));
//        boolean czyParzysta = czyLiczbaJestParzysta(a);
//        System.out.println(czyParzysta);
        }
        /////////////////////////////////////////////////////

        public static void drukujParzystoscLiczby(int parametr) {
            if (parametr % 2 == 0) {
                System.out.println("Liczba jest parzysta");
            } else {
                System.out.println("Liczba nie jest parzysta");
            }
        }

//    static boolean czyLiczbaJestParzysta(int d) {
//        if (d % 2 == 0) {
//            return true;
//        }
//        return false;
//    }


        static boolean czyLiczbaJestParzysta(int d) {
            return d % 2 == 0;
        }
    }

