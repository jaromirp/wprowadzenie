package pl.sda.wprowadzenie.przykladyCzasuDaty;

//Napisz aplikację, która obliczy twój wiek.

import java.time.LocalDate;
import java.time.Period;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import static java.time.LocalDate.*;

public class DataCzasILeMaszLat {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rok urodzenia:");

        String dataStringRokUr = scanner.next();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dataRokUr = parse(dataStringRokUr, formatter);

        LocalDate teraz = LocalDate.now();
        //S//ystem.out.println("Data Urodzenia wynosi: ", dateUr);
        Period period2 = Period.between(dataRokUr, teraz);
        System.out.println(period2.getYears()+ " " +period2.getMonths());// "years: , months: , days:

        //String odpowiedz;
    }
}
