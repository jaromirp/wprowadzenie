package pl.sda.domowe.zadan6;

public class Zadanie6 {
    public static void main(String[] args) {
        //Zadeklaruj dwie zmienne - 'waga' oraz 'wzrost'.
        //        Przypisz do nich jakieś wartości.
        //        Stwórz instrukcję warunkową ('if') który sprawdza czy osoba
        //(np. taka która  na kolejkę/rollercoaster) jest wyższa niż 150 cm wzrostu i nie przekracza wagą 180 kg.

        int waga;
        int wzrost;

        waga = 180;
        wzrost = 151;

        if (waga <= 180 && wzrost > 150) {
            System.out.println("Wzrost i Waga OK");
        } else {
            System.out.println("Wzrost i/lub waga NOT OK");
        }

    }

}