package pl.sda.domowe.zadan8C;

import java.util.Scanner;

public class Zadanie8C {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);


        int liczba;
        int[] tablicaLiczb = new int[5];
        int licznikIndeksu = 0;
        do {
            liczba = scanner.nextInt();

            // zabezpieczenie przed wykroczeniem poza tablicę
            if (tablicaLiczb.length <= licznikIndeksu) {
                // rozszerzamy tablicę
                int[] nowaTablica = new int[tablicaLiczb.length * 2];
                // przepisujemy elementy
                for (int i = 0; i < tablicaLiczb.length; i++) {
                    nowaTablica[i] = tablicaLiczb[i];
                }
                // przypisanie nowego elementu
                nowaTablica[licznikIndeksu] = liczba;

                // zwiększenie indeksu
                licznikIndeksu++;
                // zastąpienie starej tablicy nową tablica
                tablicaLiczb = nowaTablica;
            } else {
                // jeśli wystarczy nam miejsca w obecnej tablicy
                tablicaLiczb[licznikIndeksu] = liczba;
                licznikIndeksu++;
            }

        } while (liczba != 0);

        int min, max;

        min = tablicaLiczb[0];
        max = tablicaLiczb[0];
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            if (tablicaLiczb[i] < min) {
                min = tablicaLiczb[i];
            }
            if (tablicaLiczb[i] > max) {
                max = tablicaLiczb[i];
            }
        }

        System.out.println(min + max);

        int suma = 0;
        for (int i = 0; i < licznikIndeksu - 1; i++) {
            suma += tablicaLiczb[i];
        }
        System.out.println((suma / (double) (licznikIndeksu - 1)));

    }
}


