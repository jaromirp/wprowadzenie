package pl.sda.wprowadzenie.metodyZadania;

public class MetodyZadanie2B {
    public static void main(String[] args) {

        int b = 15;
        int podajLiczbeDzielenia = 2; //Podzielność
        boolean wynik = metoda3lub5(b, podajLiczbeDzielenia);
        System.out.println(wynik);
    }

    public static boolean metoda3lub5 (int b, int dzielnik) {
        return (b % dzielnik == 0);

    }
}