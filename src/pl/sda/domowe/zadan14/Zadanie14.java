package pl.sda.domowe.zadan14;

//Rozwiąż to zadanie wykorzystując mechanizm losowania liczb z klasy Random oraz z klasy Math (metoda random()).

//Zadanie14. Napisz aplikację która dla podanej wartości przez użytkownika:
//zwraca jej wartość bezwzględną.
//zwraca jej wartość przeciwną
//zwraca jej wartość odwrotną

//import java.util.Random;

import java.util.Scanner;

public class Zadanie14 {
    public static void main(String[] args) {

        /*
         * Przykład pierwiastek i wartość bezwzględna.
         */
        //int n;
        //Scanner in = new Scanner(System.in);


        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę n:");
        //int N = scanner.nextInt();
        int n = scanner.nextInt();

        //      Random generator = new Random();

        /*
         * WARTOŚĆ BEZWZGLĘDNA
         */
        System.out.println();
        // Wartość bezwzględna, to metoda Math.abs(d)
        //    System.out.println("Podaj liczbę ujemną lub dodatnią");
        //int n = in.nextInt();
        //int nDodatnia = Math.abs(n);
        //int nDodatnia = Math.random(n); // Do rozwiązania
        //System.out.println("Wartość bezwzględna z " + n + " wynosi " + (n*-1)); // + nDodatnia);

        //System.out.println();

        /*
         * TRÓJARGUMENTOWY OPERATOR WARUNKOWY
         */
        // Wartość bezwzględna bez metody Math.abs(d)
        // Zastosowana trójargumentowy operator warunkowy:
        // WARUNEK ? JEŚLI_PRAWDA : JEŚLI_FAŁSZ;
        //nDodatnia = n > 0 ? n : -n;
        if (n < 0) {
            System.out.println("Wartość bezwzględna z liczby ujemnej n: " + n + " wynosi: " + (n * -1)); //+ nDodatnia);
        } else if (n >= 0) {
            System.out.println("Wartość bezwzględna z liczby dodatniej n:" + n + " wynosi" + n);
        }

        if (n < 0) {
            System.out.println("Wartość przeciwna do wartości n= " + (n = n * (-1)));
        } else if (n > 0) {
            System.out.println("Wartość przeciwna do wartości n= " + (n = n * (-1)));
        }
        if (n < 0) {
            System.out.println("Wartość odwrotna do wartości n= " + n + "wynosi: " + (n = n * (-1)));
            System.out.println(n = n * (-1));
        } else if (n > 0) {
            System.out.println("Wartość odwrotna do wartości n= " + n + "wynosi: " + (n = n * (-1)));
            System.out.println(n = n * (-1));
        }
    }
}

