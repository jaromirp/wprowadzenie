package pl.sda.domowe.zadan12;

import java.util.Scanner;

public class zadan12 {

    public static void main(String[] args) {
        //Zadeklaruj dwie zmienne - 'waga' oraz 'wzrost'.
        //        Przypisz do nich jakieś wartości.
        //        Stwórz instrukcję warunkową ('if') który sprawdza czy osoba
        //(np. taka która  na kolejkę/rollercoaster) jest wyższa niż 150 cm wzrostu i nie przekracza wagą 180 kg.
        // Zadanie 12
        //(Wymaga umiejętności korzystania ze Scanner’a).
        // Zmodyfikuj zadanie z rollercoasterem.
        // Pobierz parametry waga, wzrost, wiek od użytkownika.
        // Zweryfikuj poprawność danych (większe od zera itp.),
        // a następnie sprawdź czy Twoja aplikacja działa poprawnie.

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj Wagę: ");
        int waga = scanner.nextInt();

        System.out.println("Podaj Wzrost: ");
        int wzrost = scanner.nextInt();

        System.out.println("Podaj Wiek: ");
        int wiek = scanner.nextInt();

        if (waga <= 180 && waga != 0 && wzrost > 150 && wzrost != 0 && wiek >= 18 && wiek != 0) {
            System.out.println("Wzrost i Waga OK można wejść :)");
        } else {
            //waga <= 180 && waga != 0 && wzrost > 150 && wzrost != 0 && wiek >= 18 && wiek != 0
            System.out.println();
            System.out.print(waga);
            System.out.print(wzrost);
            System.out.print(wiek);
            System.out.println();
            System.out.println("Wzrost i/lub waga i/lub NOT OK lub podano wartości Zero");
        }

    }

}


