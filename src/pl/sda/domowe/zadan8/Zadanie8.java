package pl.sda.domowe.zadan8;

public class Zadanie8 {
    public static void main(String[] args) {
        float ocena_matematyka = 0.0f;
        float ocena_chemia = 0.0f;
        float ocena_j_polski = 0.0f;
        float ocena_j_angielski = 0.0f;
        float ocena_wos = 0.0f;
        float ocena_informatyka = 0.0f;

        ocena_matematyka = 6;
        ocena_chemia = 6;
        ocena_informatyka = 5;
        ocena_j_polski = 2;
        ocena_j_angielski = 5;
        ocena_wos = 5;

        if (ocena_matematyka > 1.0f && ocena_chemia > 1.0f && ocena_informatyka > 1.0f && ocena_j_polski > 1.0f
                && ocena_j_angielski > 1.0f && ocena_wos > 1.0f) {
            System.out.println("Średnia z wszystich ocen: "
                    + ((ocena_matematyka + ocena_chemia + ocena_informatyka + ocena_j_polski + ocena_j_angielski + ocena_wos) / 6));
            System.out.println("Średnia ocen z przedmiotów ścisłych: matematyka, chemia, informatyka: " + ((ocena_matematyka + ocena_chemia + ocena_informatyka) / 3));
            System.out.println("Średnia ocen z przedmiotów humanistycznych (pozostałe): " + ((ocena_j_polski + ocena_j_angielski + ocena_wos) / 3));
        } else
            System.out.println("Ocena z [część] jest niedostateczna");
    }
}


// Napisz aplikację której zadaniem jest liczenie srednich ocen z kilku przedmiotów.
//     - ocena_matematyka
//   - ocena_chemia
//   - ocena_j_polski
//   - ocena_j_angielski
//     - ocena_wos
//      - ocena_informatyka
//
//Aplikacja ma wyliczac średnią wszystkich ocen, srednią ocen z przedmiotów ścisłych (matematyka, chemia, informatyka),
// oraz średnią z ocen przedmiotów humanistycznych (pozostałe).
//  Wszystkie trzy średnie mają być wypisane na ekran. Zwróć uwagę na zaokrąglenia.
//  Jeśli którakolwiek z ocen jest niedostateczna, lub średnia z ocen z którejś części jest niedostateczna, to wyświetl napis:
//
//    Ocena z [część] jest niedostateczna.