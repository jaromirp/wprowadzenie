package pl.sda.wprowadzenie.OOP;

public class Komputer {
    public int getGetPotrzebnaMoc() {
        return getPotrzebnaMoc;
    }

    public void setGetPotrzebnaMoc(int getPotrzebnaMoc) {
        this.getPotrzebnaMoc = getPotrzebnaMoc;
    }

    public void setProducent(String producent) {
        this.producent = producent;
    }

    public void setTypProcesora(TypProcesora typProcesora) {
        this.typProcesora = typProcesora;
    }

    public String getProducent() {
        return producent;
    }

    public TypProcesora getTypProcesora() {
        return typProcesora;
    }

    public Komputer(int getPotrzebnaMoc, String producent, TypProcesora typProcesora) {
        this.getPotrzebnaMoc = getPotrzebnaMoc;
        this.producent = producent;
        this.typProcesora = typProcesora;
    }

    int getPotrzebnaMoc;
    String producent;
    TypProcesora typProcesora;

}
