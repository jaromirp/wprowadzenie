package pl.sda.domowe.zadan11_Scanner;

import java.util.Scanner;

//Scanner ! :)

//Napisać program obliczający należny podatek dochodowy od osób ﬁzycznych.
// Program ma [(jeśli poznaliśmy scanner) pobierać od użytkownika/
// (jeśli nie poznaliśmy scannera) pobierać wartość ze zmiennej] dochód i po obliczeniu wypisywać na ekranie należny podatek.
// Podatek obliczany jest wg. następujących reguł:
// do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
//od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad 85.528,00

public class Zadanie11b_Scanner {
    public static void main(String[] args) {

        float progPodakowy = 85528; //%.2f,

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj kwotę przychodu: ");
        float kwotaPrzychodu = scanner.nextInt();

        //System.out.println("Podaj liczbe do ktorej iterujemy:");
        //int liczba = scanner.nextInt();

        if (kwotaPrzychodu <= progPodakowy) {

            System.out.println("Pierwszy próg podatkowy: ");
            System.out.println((kwotaPrzychodu * 0.18) - 556.02);
        } else {
            System.out.println("Drugi próg podatkowy: " + (14839.02 + (kwotaPrzychodu - progPodakowy) * 0.32));
        }
    }
}
