package pl.sda.domowe.zadan10;

//Zadeklaruj trzy zmienne (liczby).
// Wypisz na ekran wszystkie z nich, a następnie wypisz na ekran największą oraz najmniejszą z nich.

public class Zadanie10 {
    public static void main(String[] args) {
        int liczba1 = 11200;
        int liczba2 = 2500;
        int liczba3 = 14000;

        //Dla liczby najwięszej
        if (liczba1 > liczba2 && liczba1 > liczba3) {
            System.out.println(liczba1);
        }

        if (liczba2 > liczba1 && liczba2 > liczba3) {
            System.out.println(liczba2);
        }

        if (liczba3 > liczba1 && liczba3 > liczba2) {
            System.out.println(liczba3);
        }

        //Dla liczby najmniejszej
        if (liczba1 < liczba2 && liczba1 < liczba3) {
            System.out.println(liczba1);
        }

        if (liczba2 < liczba1 && liczba2 < liczba3) {
            System.out.println(liczba2);
        }

        if (liczba3 < liczba1 && liczba3 < liczba2) {
            System.out.println(liczba3);
        }


        // Inne rozwiązenie:
        int liczbaMax = Math.max(liczba1, Math.max(liczba2, liczba3));
        System.out.println(liczbaMax);

        int liczbaMin = Math.min(liczba1, Math.min(liczba2, liczba3));
        System.out.println(liczbaMin);

    }
}
