package pl.sda.domowe.zadan8_B;

//import java.util.Random;

import java.util.Scanner;

public class Zadanie8_B {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int liczba;
        do {
            System.out.println("Podaj liczbę:");
            liczba = scanner.nextInt();

        } while (liczba < 0);


        // 2 ^ 0
        int wypisywana = 1;

        do {
            System.out.println(wypisywana);
            wypisywana *= 2;
        } while (wypisywana < liczba);


    }
}

