package pl.sda.wprowadzenie.wyjatki;

import java.util.Scanner;

public class Zad2Wyjatki {
    //Stwórz program obliczający równanie kwadratowe (http://matematyka.pisz.pl/strona/54.html).
    // Na wejściu użytkownik podaje wartość zmiennych A, B i C, a na wyjściu wypisują sie odpowiednie komunikaty
    // o wartościach x1 i x2 lub x0, a także wartość delty. Jeśli dla podanych wartości delta jest ujemna,
    // rzuć stworzony przez siebie wyjątek.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę A");
        int A = scanner.nextInt();
        System.out.println("Podaj liczbę B");
        int B = scanner.nextInt();
        System.out.println("Podaj liczbę C");
        int C = scanner.nextInt();

        int delta;
        delta = ((B * B) - (4 * A * C));
        if (delta > 0) {
            System.out.println("Wynik delta = " + delta);
        } else if (delta == 0) {
            System.out.println("Podano Wartości zero, powtórz");
        } else {
            System.out.println("wartość mniejsza od zera");


            try {
                if (delta < 0) {
                    throw new RuntimeException("delta<0");

                }

                System.out.println();
            } catch (Exception E) {
                System.out.println("Blad " + E.getMessage());
            }


        }

    }
}

