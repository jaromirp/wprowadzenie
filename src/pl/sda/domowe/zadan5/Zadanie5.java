package pl.sda.domowe.zadan5;

public class Zadanie5 {
    public static void main(String[] args) {
        boolean jest_cieplo = true;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = true;

        //5 Zadeklaruj zmienne logiczne (boolean) które noszą nazwe:
        //jest_cieplo
        //        wieje_wiatr
        //swieci_slonce

        //oraz zmienne:
        //ubieram_sie_cieplo - jesli nie jest cieplo lub wieje wiatr
        //biore_parasol - jesli nie swieci slonce ale nie wieje wiatr
        //ubieram_kurtke - jesli wieje, nie ma slonca i nie jest cieplo

        boolean ubieram_sie_cieplo = !jest_cieplo || wieje_wiatr;
        //ubieram_sie_cieplo = "jesli nie jest cieplo lub wieje wiatr";

        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;
                //biore_parasol = "jesli nie swieci slonce ale nie wieje wiatr";

        boolean ubieram_kurtke = wieje_wiatr&& !swieci_slonce &&!jest_cieplo;
        //ubieram_kurtke = "jesli wieje, nie ma slonca i nie jest cieplo";

        if(ubieram_kurtke){
            System.out.println("Ubieram kurtke");
        }
        if(biore_parasol){
            System.out.println("Biorę parasol");
        }

        if(ubieram_sie_cieplo){
            System.out.println("Ubieram się ciepło");
        }
    }
}