package pl.sda.wprowadzenie.metodyZadania;

public class MetodyZadanie2 {
        public static void main(String[] args) {

            int b = 15;
            boolean wynik = metoda3lub5(b);
            System.out.println(wynik);
    }

    public static boolean metoda3lub5 (int b) {
            return (b % 3 == 0 && b % 5 == 0);

    }
}

