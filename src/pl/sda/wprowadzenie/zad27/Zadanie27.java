package pl.sda.wprowadzenie.zad27;

import java.util.Random;
import java.util.Scanner;

public class Zadanie27 {
    public static void main(String[] args) {
        //Przyklad DoWhile
        Scanner scanner = new Scanner(System.in);

        //
        //Zaimplenetuj grę w zgadywankę
        //Wylosuj liczbę (użyj do tego klasy Random) z zakresu do 100.
        //Użytkownik ma podawać liczbę dopóki nie odgadnie wylosowanej liczny
        //
        //Wykorzystaj pętlę do...while

        Random generator = new Random();
        int wygenerowana = generator.nextInt(100);

        int liczbaOdUzytkownika;
        do {
            System.out.println("Podaj liczbę:");
            liczbaOdUzytkownika = scanner.nextInt();

        } while (liczbaOdUzytkownika != 0);
    }
}