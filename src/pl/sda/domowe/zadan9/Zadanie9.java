package pl.sda.domowe.zadan9;

public class Zadanie9 {
    public static void main(String[] args) {
        //Napisać program służący do konwersji wartości temperatury podanej w stopniach Celsjusza na stopnie w
        //skali Fahrenheita        (stopnie Fahrenheita = 1.8 * stopnie Celsjusza + 32.0)
        // °F = (°C × 1.8) + 32  Zmiana Celcusza na Farenhajta
        // °C = (°F − 32) /1.8   Zmiana Farenhajta na Celcjusza

        float Stopnie_celcujsza = 32.0f;
        float Stopnie_Farenhajta = 1.8f;

        System.out.println("ZmianaNaStopnieCelcjusza: " + ((Stopnie_celcujsza * 1.8) + 32));
        System.out.println("ZmianaNaStopnieFarenhajta: " + ((Stopnie_Farenhajta - 32) / 1.8));
    }
}
