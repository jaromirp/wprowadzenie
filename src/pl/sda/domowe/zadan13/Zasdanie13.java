package pl.sda.domowe.zadan13;

import java.util.Random;
import java.util.Scanner;

//Zadanie 13
//Napisz aplikację która pobiera od użytkownika N a nastepnie:
//        losuje N liczb całkowitych (dowolny zakres) i wypisuje je na ekran
//        losuje N liczb zmiennoprzecinkowych i wypisuje je na ekran
//        losuje N razy wartość boolean i wypisuje je na ekran
//        pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb całkowitych z tego zakresu,
//          a następnie wypisuje je na ekran
//        pobiera kolejne dwa parametry poczatekZakresu i koniecZakresu i losuje N liczb zmiennoprzecinkowych z tego zakresu,
//          a następnie wypisuje je na ekran

public class Zasdanie13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj N:");
        int N = scanner.nextInt();

        Random generator = new Random();

        // A
        for (int i = 0; i < N; i++) {
            int b = generator.nextInt();
            System.out.println(b);
        }

        // B
        for (int i = 0; i < N; i++) {
            double b = generator.nextDouble();
            System.out.println(b);
        }

        // C
        for (int i = 0; i < N; i++) {
            boolean b = generator.nextBoolean();
            System.out.println(b);
        }

        // D
        int poczatekZakresu, koniecZakresu;
        System.out.println("Podaj poczatek zakresu:");
        poczatekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        koniecZakresu = scanner.nextInt();

        for (int i = 0; i < N; i++) {
            int b = generator.nextInt(koniecZakresu - poczatekZakresu + 1) + poczatekZakresu;
            System.out.println(b);
        }

        // E
        for (int i = 0; i < N; i++) {
            double b = generator.nextDouble() * (koniecZakresu - poczatekZakresu + 1) + poczatekZakresu;
            System.out.println(b);
        }
    }
}

