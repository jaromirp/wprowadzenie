package pl.sda.wprowadzenie.zad7;

public class Zadanie7 {
    public static void main(String[] args) {

        if (2 > 3) {
            System.out.println("Spełniony 2>3");
        } else {
            System.out.println(" Nie spełniony 2>3");

            if (4 < 5) {
                System.out.println("Spełniony 4<5");
            } else {
                System.out.println(" Nie spełniony 4<5");
            }
            if ((2 - 2) == 0) {
                System.out.println("Spełniony (2-2)==0");
            } else {
                System.out.println(" Nie spełniony (2-2)==0");
            }

            boolean zmienna = true;
            int wartosc;
            if (zmienna) {
                System.out.println("Spełniony true");
            } else {
                System.out.println(" Nie spełniony true");

            }

            if ((9 % 2) == 0) {
                System.out.println("Spełniony (9%2)==0");
            } else {
                System.out.println(" Nie spełniony (9%2)==0");

                if ((9 % 3) == 0) {
                    System.out.println("Spełniony (9%3)==0");
                } else {
                    System.out.println(" Nie spełniony (9%3)==0");

                }
            }
        }
    }
}
