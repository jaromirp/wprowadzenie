package pl.sda.domowe.zadanie_Gra_Lotto_B;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ZadanieGraLotto16_B {
    public static void main(String[] args) {

        int[] typowaneLiczby = new int[6];

        typujLiczbyUzytkownika(typowaneLiczby);
        System.out.println(Arrays.toString(typowaneLiczby));

        int[] wylosowaneLiczby = new int[6];
        losujLiczby(wylosowaneLiczby);
        System.out.println("Wylosowane liczby to :\n"
                + Arrays.toString(wylosowaneLiczby));
    }

    private static void losujLiczby(int[] liczby) {
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            liczby[i] = random.nextInt(48) + 1;
        }
    }

    private static void typujLiczbyUzytkownika(int[] typowaneLiczby) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj 6 liczb");
        for (int i = 0; i < 6; i++) {
            typowaneLiczby[i] = scanner.nextInt();
        }
    }
}
