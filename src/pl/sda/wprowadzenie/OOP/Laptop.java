package pl.sda.wprowadzenie.OOP;

public class Laptop extends Komputer {
    private double wielkoscMatrycy;
    private boolean czyPosiadaRetine;

    public Laptop(int getPotrzebnaMoc, String producent, TypProcesora typProcesora) {
        super(getPotrzebnaMoc, producent, typProcesora);
    }

    public double getWielkoscMatrycy() {
        return wielkoscMatrycy;
    }

    public void setWielkoscMatrycy(double wielkoscMatrycy) {
        this.wielkoscMatrycy = wielkoscMatrycy;
    }

    public void setCzyPosiadaRetine(boolean czyPosiadaRetine) {
        this.czyPosiadaRetine = czyPosiadaRetine;
    }

    public boolean isCzyPosiadaRetine() {
        return czyPosiadaRetine;
    }

}


