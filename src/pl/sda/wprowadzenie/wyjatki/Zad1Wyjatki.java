package pl.sda.wprowadzenie.wyjatki;

//import java.util.Scanner;

import java.util.Scanner;

public class Zad1Wyjatki {
    //Napisz program, który prosi użytkownika o dwie liczby a i b, i wyświetla wynik dzielenia a/b.
    // Jeżeli b jest ujemne program powinien wyświetlić odpowiedni komunikat.
    //Wariant 1: Zastosuj instrukcję if
    //Wariant 2: Zastosuj instrukcję try-catch

//    Scanner scanner = new Scanner(System.in);
//    System.out.println("Podaj liczbę a");
//    String a = scanner.next();
//        System.out.println("Podaj liczbę b");
//    String b = scanner.next();


    public static void main(String[] args) {
        //metoda_sposob1(1, -1);  //zachaszowano tak było wcześniej bez scanner'a
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę a");
        int a = scanner.nextInt();
        System.out.println("Podaj liczbę b");
        int b = scanner.nextInt();
        System.out.println();

        //static void metoda_sposob (a, b);
        //   int wynik;
//
//        wynik = a / b;
//        System.out.println(wynik);
        //     metoda_sposob1(int a, int b);

//        try {
        //metoda_sposob2(1, -1);  // to mozesz odkomentowac
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Wystąpił błąd, treść błędu: " + e.getMessage());
//        }


        //( int a, int b){
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj liczbę a");
//        int a = scanner.nextInt();
//        System.out.println("Podaj liczbę b");
//        int b = scanner.nextInt();
//        System.out.println();
        int wynik;

        wynik = a / b;
        System.out.println(wynik);

        try {
            if (b < 0) {
                throw new Exception("B < 0");
            }
        } catch (Exception e) {
            // co ma się stać jeśli błąd wystąpi
            System.out.println("Wystąpił błąd, treść błędu: " + e.getMessage());
        }
    }

    private static void metoda_sposob2(int a, int b) throws Exception {
        if (b < 0) {
            throw new Exception("B < 0");
        }
    }
}


