package pl.sda.domowe.zadan7;

public class Zadanie7 {
    public static void main(String[] args) {
        //Zadeklaruj dwie zmienne - 'waga' oraz 'wzrost'.
        //        Przypisz do nich jakieś wartości.
        //        Stwórz instrukcję warunkową ('if') który sprawdza czy osoba
        //(np. taka która  na kolejkę/rollercoaster) jest wyższa niż 150 cm wzrostu i nie przekracza wagą 180 kg.

        //Rozwinięcie poprzedniego zadania:
        //Dopisz do poprzedniej aplikacji dodatkową zmienną - wiek.
        //        Jeśli osoba jest młodsza niż 10 lat, lub starsza niż 80, to nie może wejść na kolejkę.
        //       Dopisz/zmień do/w poprzedniej aplikacji - osoba może wejść na kolejkę jeśli jej wzrost jest od 150 do 220 cm wzrostu.
        //Dopisz deskryptywne wyjasnienia.
        //        Jesli osoba nie moze wejsc na kolejke, to wypisz na konsole odpowiedni komunikat dlaczego.
        // np. “Nie możesz wejść na kolejkę ponieważ jesteś zbyt ciężki/a”
        //Jesli osoba nie moze wejsc z powodu wagi, to powinien sie wypisac komunikat ze nie moze wejsc bo przekracza limit wagowy
        //Jesli osoba nie moze wejsc z powodu wieku, to powinien sie wypisac INNY komunikat o tym ze nie moze wejsc z powodu wieku.


        int waga;
        int wzrost;
        int wiek;

        waga = 151;
        wzrost = 180;
        wiek = 50;

        // if (waga >180)

        if (wzrost >= 150 && wzrost <= 220 && waga <= 180 && wiek >= 10 && wiek <= 80) {
            System.out.println("Wzrost i Waga i Wiek OK Możesz Wejść");
        } else {
            if (wzrost < 150 && wzrost <= 220) {
                System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt niski/a");
            } else {
                if (wzrost <= 220) {
                    System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt wysoki/a");
                } else {
                    if (waga <= 180) {
                        System.out.println("Nie możesz wejść na kolejkę ponieważ zbyt ciężki/a");

                    } else {
                        if (wiek < 10) {
                            System.out.println("Nie możesz wejść na kolejkę ponieważ jesteś zbyt młody");
                        } else {
                            {
                                if (wiek > 80) {
                                    System.out.println("Nie możesz wejść na kolejkę ponieważ wiek przekracza 80 lat");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

