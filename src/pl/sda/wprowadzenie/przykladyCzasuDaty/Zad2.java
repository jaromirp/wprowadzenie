package pl.sda.wprowadzenie.przykladyCzasuDaty;


// Napisz aplikację, która wyświetli datę sprzed 10 dni i datę za dziesięć dni.
//Wskazówka: skorzystaj z metody plusDays() na obiekcie LocalDate.

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {
     String czas2;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę");
        String data2 = scanner.next();


        //System.out.println(data2);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        LocalDate dateZTekstu = LocalDate.parse(data2, dateTimeFormatter);
        System.out.println(dateZTekstu.plusDays(10));
        System.out.println(dateZTekstu.plusDays(-10));

    }


}
