package pl.sda.wprowadzenie.zad5;

public class Zadanie5 {
    public static void main(String[] args) {
        System.out.println(false==false);
        System.out.println(false!=true);
        System.out.println(!true);
        System.out.println(2>4);
        System.out.println(3>5);
        System.out.println(3==3&&3==4);
        System.out.println(3!=5||3==5);
        System.out.println((2+4)>(1+3));
        System.out.println("cos".equals("cos"));
        System.out.println("cos"=="cos");

        //Scanner scanner = new Scanner(System.in);
        //String zmienna = "cos"; // predefiniowane miejsce w pamięci
        //String zmienna2 = scanner.next(); // miejsce w pamieci z konsoli/klawiatury

        //System.out.println(zmienna.equals("cos")); // porównanie treści
        //System.out.println(zmienna == zmienna2); // porównanie referencji

    }
}
