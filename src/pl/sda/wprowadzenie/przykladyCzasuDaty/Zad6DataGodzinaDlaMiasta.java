package pl.sda.wprowadzenie.przykladyCzasuDaty;

// Zad6(DOMOWE) Napisz aplikację, ktora po podaniu przez użytkownika miasta, wypisuje obecną datę i godzinę w wybranym mieście.
//   Miasta: Warszawa, Paryż, Whitehorse, Moskwa, Canberra, Sosnowiec.
//   Wykorzystaj "ZoneId".
//    https://www.timeanddate.com/time/map/
//
//        Map<String,String> zoneIds = new HashMap<>();
//        zoneIds.put("Warszawa", "Europe/Warsaw");
//        zoneIds.put("Paryż", "Europe/Paris");
//        zoneIds.put("Whitehorse", "Etc/UTC");
//        zoneIds.put("Moskwa", "Europe/Moscow");
//        zoneIds.put("Canberra", "Australia/Sydney");
//        zoneIds.put("Sosnowiec", "Europe/Warsaw");
//
//    String timezone = zoneIds.get("Moskwa");
//    ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of(timezone));

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zad6DataGodzinaDlaMiasta {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Napisz nazwę miasta, która Ciebie interesuje z listy: Warszawa, Paryż, Whitehorse, Moskwa, Canberra, Sosnowiec, Gdańsk");

        //String dataString = scanner.next();
        String miasto = scanner.next(); //tutaj podanie nazwy miasta przez Użytkownika
        System.out.println("Data i Godzina dla wpisanego miasta wg lokalizacji: ");

        Map<String, String> zoneIds = new HashMap<>();
        zoneIds.put("Warszawa", "Europe/Warsaw");
        zoneIds.put("Paryż", "Europe/Paris");
        zoneIds.put("Whitehorse", "Etc/UTC");
        zoneIds.put("Moskwa", "Europe/Moscow");
        zoneIds.put("Canberra", "Australia/Sydney");
        zoneIds.put("Sosnowiec", "Europe/Warsaw");
        zoneIds.put("Gdańsk", "Europe/Warsaw");  //dodatkowy rekord

        //String timezone = zoneIds.get("Moskwa"); //string z treści zadania
        //String timezone = zoneIds.get(String.valueOf(miasto)); //tak też można zapisać dla rekordu poniżej
        String timezone = zoneIds.get(miasto);
        ZonedDateTime dateTime = ZonedDateTime.now(ZoneId.of(timezone));

        System.out.println(dateTime);
        }
}

