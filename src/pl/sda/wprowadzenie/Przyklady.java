package pl.sda.wprowadzenie;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Przyklady {
//    Stwórz aplikację która w pętli przyjmuje polecenie:
//            - wpisanie date wypisuje obecny LocalDate
//        - wpisanie time wypisuje obecny LocalTime
//        - wpisanie datetime wypisuje obecny LocalDateTime
//            (W wybranym przez Ciebie formacie)
//    Jeśli użytkownik wpisze 'quit' to zakoncz program.

    public static void main(String[] args) {
        String czas;

        do {

            Scanner scanner = new Scanner(System.in);
            czas = scanner.next();

            switch (czas) {

                case "date":
                    //  LocalDate  --> wpisz:  date
                    LocalDate localDate = LocalDate.now();
                    System.out.println(localDate);

                    break;
                case "time":
                    //Local time   --> wpisz:  time
                    LocalTime localTime = LocalTime.now();
                    System.out.println(localTime);
                    break;

                case "datetime":
                    //LocalDateTime  --> wpisz:  datetime
                    LocalDateTime localDateTime = LocalDateTime.now();
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/mm/dd HH:mm:ss");
                    System.out.println(localDateTime.format(dateTimeFormatter));
                    break;

            }
        }
        while (!czas.equals("quit"));

    }
}



