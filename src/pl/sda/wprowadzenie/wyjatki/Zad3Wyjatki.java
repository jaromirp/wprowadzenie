package pl.sda.wprowadzenie.wyjatki;

//Napisz program który pyta użytkownika o dwie liczby a następnie sprawdza czy druga liczba nie jest ==0.
// Jeśli jest, to wyrzuca wyjątek ‘CholeroException’ z komunikatem “Pamiętaj cholero, by nie dzielić przez zero!”.

import java.util.Scanner;

public class Zad3Wyjatki {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę D");
        int D = scanner.nextInt();
        System.out.println("Podaj liczbę F");
        int F = scanner.nextInt();
        System.out.println("Liczba D = " + D);
        System.out.println();

        if (D >=0 && F > 0) {
            //System.out.println("Liczba D = ");
            System.out.println("Liczba F = " + F);
            System.out.println("Wynik dzielenia "+ D/F);
        }

        try {
            if (F < 0) {
                throw new Exception("F < 0");

            }
            System.out.println();
        } catch (Exception e)        {
            System.out.println("Pamiętaj cholero, by nie dzielić przez zero! " + e.getMessage());
        }

    }
}


