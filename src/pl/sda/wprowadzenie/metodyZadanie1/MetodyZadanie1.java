package pl.sda.wprowadzenie.metodyZadanie1;

public class MetodyZadanie1 {
    public static void main(String[] args) {

        int x = 1001;
        drukujParzystoscLiczby(x);
    }

    // drukuj Liczby Parzyste
    public static void drukujParzystoscLiczby(int x) {
        if (x % 2 == 0) {
            System.out.println("Liczba parzysta");
        } else {
            System.out.println("Liczba nie jest parzysta");
        }
    }

    static boolean czyLiczbaJestParzysta(int d) {
        if (d % 2 == 0) {
            return true;
        }
        return false;

    }

//    static boolean czyLiczbaJestParzysta(int d) {
//        if d % 2 == 0
//        return true;
    }



